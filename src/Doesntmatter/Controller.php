<?php


namespace Doesntmatter;


class Controller
{
    /**
     * @param string $msg
     * @return string
     */
    protected function printFoo(string $msg = ''): string
    {
        return $msg ?? get_class();
    }
}